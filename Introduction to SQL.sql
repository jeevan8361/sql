select * from dataset;
select min(postal) as minimum from dataset;
select max(postal) as maximum from dataset;

select count(customername) as counts from dataset where country='India';
select avg(postal) as average from dataset;
select sum(customerid) as sum from dataset;

select customername as name from dataset where city like '%rr_';
select mobile as phone from dataset where mobile like '%55';

select * from dataset where country in ('USA','India');
select * from dataset where country not in ('USA','India');

select * from dataset where customerid between 51 and 53;
select * from dataset where customerid not between 50 and 52;
select * from dataset where customername between 'harry' and 'victor' order by customername;
select * from dataset where customername not between 'harry' and 'john' order by customername;

select customername as name, concat_ws(',',city,postal,country) as address from dataset;
